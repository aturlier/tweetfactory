//
//  ViewController.h
//  TweetFactory
//
//  Created by Axel Turlier on 03/11/2016.
//  Copyright © 2016 Axel Turlier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *customTweetInput;
@property (weak, nonatomic) IBOutlet UIPickerView *prefabTweetPickerView;

@end

