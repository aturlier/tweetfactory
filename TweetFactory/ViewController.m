//
//  ViewController.m
//  TweetFactory
//
//  Created by Axel Turlier on 03/11/2016.
//  Copyright © 2016 Axel Turlier. All rights reserved.
//

#import "ViewController.h"
#import <Twitter/Twitter.h>
#import <TwitterKit/TwitterKit.h>
#import <TwitterCore/TwitterCore.h>

@interface ViewController ()

@property(nonatomic, strong) NSArray *actions;
@property(nonatomic, strong) NSArray *emotions;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _actions = [[NSArray alloc] initWithObjects:@"dors", @"mange", @"suis en cours", @"galère", @"cours", @"poireaute", nil];
    _emotions = [[NSArray alloc] initWithObjects:@"😉", @"🙂", @"😞", @"😮", @"😀", @"😂", @"😆", nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : UIPIcker Delegate & Data Source
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return  [(component == 0 ? _actions : _emotions) objectAtIndex:row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [(component ==  0 ? _actions : _emotions) count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

#pragma mark : Actions
- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)sendTweet:(id)sender {
    
    NSString *toSend;
    
    if([_customTweetInput hasText]){
        toSend = [NSString stringWithString:_customTweetInput.text];
    }else{
        toSend = [NSString stringWithFormat:
                                              @"Je %@ %@",
                                              [_actions objectAtIndex:[_prefabTweetPickerView selectedRowInComponent:0]],
                                              [_emotions objectAtIndex:[_prefabTweetPickerView selectedRowInComponent:1]]];
    }
    
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:toSend];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}


@end
