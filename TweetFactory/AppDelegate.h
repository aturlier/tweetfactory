//
//  AppDelegate.h
//  TweetFactory
//
//  Created by Axel Turlier on 03/11/2016.
//  Copyright © 2016 Axel Turlier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

