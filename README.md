# README #

App done during objc 101

### Description ###

* Quick Tweet writing (prefabs or custom)
* Version 1.0

### How do I get set up? ###

* Clone repository
* Open a terminal in the project
* Run : 
```
#!bash
pod install

```
* There should be a Twitter Account on your iPhone/Simulator to tweet.